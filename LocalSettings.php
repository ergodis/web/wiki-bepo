<?php
# See includes/DefaultSettings.php for all configurable settings
# and their default values, but don't forget to make changes in _this_
# file, not there.

$path = array( $IP, "$IP/includes", "$IP/languages" );
set_include_path( implode( PATH_SEPARATOR, $path ) . PATH_SEPARATOR . get_include_path() );

# If PHP's memory limit is very low, some operations may fail.
# ini_set( 'memory_limit', '20M' );

if ( $wgCommandLineMode ) {
	if ( isset( $_SERVER ) && array_key_exists( 'REQUEST_METHOD', $_SERVER ) ) {
		die( "This script must be run from the command line\n" );
	}
}
## Uncomment this to disable output compression
# $wgDisableOutputCompression = true;

#$adminTask = ( PHP_SAPI === 'cli' || defined( 'MEDIAWIKI_INSTALL' ) );
#$wgReadOnly = $adminTask ? false : 'Upgrading to MediaWiki 1.35.10';

#$wgReadOnly = 'Migration vers un nouvel hebergeur!';

$wgServer = "https://bepo.fr";
$wgSitename = "Disposition de clavier francophone et ergonomique bépo";
$wgSitename = "Disposition de clavier bépo";
$wgMetaNamespace = "v2";

## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
$wgScriptPath       = "/wiki";
$wgArticlePath      = "$wgScriptPath/$1";
$wgScript           = "$wgScriptPath/index.php";
$wgUsePathInfo = false;

## For more information on customizing the URLs please see:
## http://www.mediawiki.org/wiki/Manual:Short_URL

$wgEnableEmail      = true;
$wgEnableUserEmail  = true;
$wgAllowConfirmedEmail = true;
$wgGroupPermissions['*']['edit'] = false;
$wgDisableAnonTalk = true;

$wgGroupPermissions['sysop']['userrights'] = true;
$wgGroupPermissions['sysop']['editwidgets'] = true;

$wgAllowUserCss = true;

$wgFavicon = "$wgScriptPath/favicon.ico";

$wgPasswordSender = "no-reply@bepo.fr";
$wgEmergencyContact = $wgPasswordSender;

## Trust the Reverse Proxy to give us the real user IPs
$wgUsePrivateIPs = true;
$wgCdnServers = explode(',', getenv("CC_REVERSE_PROXY_IPS"));

## For a detailed description of the following switches see
## http://www.mediawiki.org/wiki/Extension:Email_notification
## and http://www.mediawiki.org/wiki/Extension:Email_notification
## There are many more options for fine tuning available see
## /includes/DefaultSettings.php
## UPO means: this is also a user preference option
$wgEnotifUserTalk = true; # UPO
$wgEnotifWatchlist = true; # UPO
#$wgEmailAuthentication = true;

$wgDBtype           = "mysql";
//$wgDBserver         = getenv("MYSQL_ADDON_HOST");
//$wgDBport           = (int) getenv("MYSQL_ADDON_PORT");
$wgDBserver         = getenv("MYSQL_ADDON_HOST") . ":" . getenv("MYSQL_ADDON_PORT");
$wgDBname           = getenv("MYSQL_ADDON_DB");
$wgDBuser           = getenv("MYSQL_ADDON_USER");
$wgDBpassword       = getenv("MYSQL_ADDON_PASSWORD");

# MySQL specific settings
$wgDBprefix         = "mw_";

# MySQL table options to use during installation or update
$wgDBTableOptions   = "TYPE=InnoDB";

## Shared memory settings
#$wgMainCacheType = CACHE_NONE;
#$wgMemCachedServers = array();

## To enable image uploads, make sure the 'images' directory
## is writable, then set this to true:
$wgEnableUploads       = true;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";
$wgFileExtensions[]='svg';

$wgLogos = [
# ancien logo
#	'icon' => "//cdn.bepo.fr/images/logo_wiki.png",
# logo SVG (rend moins bien que le PNG)
#       'icon' => "//cdn.bepo.fr/images/Logo-scifly-2023-all-path.svg",
        'icon' => "//cdn.bepo.fr/images/Logo-2023-scifly.png",
];

# les uploads sur le site de downloads
$wgTmpDirectory = "/tmp"; #n'oubliez pas de créer le répertoire 'tmp
$wgUploadPath = "//cdn.bepo.fr/images/"; #idem


## If you want to use image uploads under safe mode,
## create the directories images/archive, images/thumb and
## images/temp, and make them all writable. Then uncomment
## this, if it's not already uncommented:
$wgHashedUploadDirectory = false;

$wgLanguageCode = "fr";

$wgSecretKey = getenv("MW_SECRET_KEY");

## Default skin: you can change the default skin. Use the internal symbolic
## names, ie 'standard', 'nostalgia', 'cologneblue', 'monobook':
wfLoadSkin( 'Vector' );
wfLoadSkin( 'MonoBook' );
wfLoadSkin( 'Timeless' );
$wgDefaultSkin = 'vector-2022';

## For attaching licensing metadata to pages, and displaying an
## appropriate copyright notice / icon. GNU Free Documentation
## License and Creative Commons licenses are supported so far.
$wgRightsPage = ""; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = "https://www.gnu.org/copyleft/fdl.html";
$wgRightsText = "GNU Free Documentation License 1.2";
$wgRightsIcon = "${wgScriptPath}/skins/common/images/gnu-fdl.png";

# When you make changes to this configuration file, this will make
# sure that cached pages are cleared.
$configdate = gmdate( 'YmdHis', @filemtime( __FILE__ ) );
$wgCacheEpoch = max( $wgCacheEpoch, $configdate );

# Ajouts manuels

//Offset pour l'heure
$wgLocaltimezone = 'Europe/Paris';
date_default_timezone_set( $wgLocaltimezone );

//Extensions
wfLoadExtension( 'AWS' );
wfLoadExtension( 'CategoryTree' );
wfLoadExtension( 'CharInsert' );
wfLoadExtension( 'Cite' );
wfLoadExtension( 'Gadgets' );
wfLoadExtension( 'ImageMap' );
wfLoadExtension( 'ParserFunctions' );
wfLoadExtension( 'Renameuser' );
wfLoadExtension( 'SyntaxHighlight_GeSHi' );
wfLoadExtension( 'TemplateStyles' );
wfLoadExtension( 'TitleKey' );
wfLoadExtension( 'VisualEditor' );
wfLoadExtension( 'Widgets' );
wfLoadExtension( 'WikiEditor' );

wfLoadExtension( 'UserMerge' );
// By default nobody can use this function, enable for bureaucrat?
$wgGroupPermissions['bureaucrat']['usermerge'] = true;

// To enable for administrators add this:
$wgGroupPermissions['sysop']['usermerge'] = true;

// optional: default is [ 'sysop' ]
$wgUserMergeProtectedGroups = [ 'sysop' ];

// Templatestyles permissions
$wgTemplateStylesAllowedUrls['image'] = [ "<^https://cdn.bepo.fr/>" ];

$wgGroupPermissions['sysop']['deletelogentry'] = true;
$wgGroupPermissions['sysop']['deleterevision'] = true;

//ConfirmEdit
wfLoadExtensions([ 'ConfirmEdit', 'ConfirmEdit/QuestyCaptcha' ]);
$wgCaptchaClass = 'QuestyCaptcha';
#special catpcha for vote
$wgCaptchaQuestions[] = array( 'question' => "Tapez trois apostrophes typographiques", 'answer' => "’’’" );
#$wgCaptchaQuestions[] = array( 'question' => "Qui est à l’origine de la disposition DSK américaine ?", 'answer' => "Dvorak" );
#$wgCaptchaQuestions[] = array( 'question' => "En toutes lettres, combien de voyelles contient la rangée de repos de la disposition bépo ?", 'answer' => "quatre" );
#$wgCaptchaQuestions[] = array( 'question' => "Quels sont les guillemets fermants en français ?", 'answer' => "»" );
#$wgCaptchaQuestions[] = array( 'question' => "En bépo, quel caractère minuscule se trouve sur la même touche que la première lettre de l’alphabet ?", 'answer' => "æ" );
#$wgCaptchaQuestions[] = array( 'question' => "En bépo, sur quelle voyelle se trouvent les diacritiques tréma et point en chef ?", 'answer' => "i" );
$wgGroupPermissions['*']['skipcaptcha'] = false;
$wgGroupPermissions['emailconfirmed']['skipcaptcha'] = true;
$wgGroupPermissions['user']['skipcaptcha'] = false;

$wgCaptchaTriggers['createaccount'] = true;
$wgCaptchaTriggers['edit'] = true;
$wgCaptchaTriggers['badlogin'] = true;
$wgCaptchaTriggers['badloginperuser'] = true;

$wgSyntaxHighlightDefaultLang = "text";

$wgCategoryTreeMaxDepth = 3;

# Modification du titre des pages
$wgRestrictDisplayTitle = false;

# Enable subpages in the main namespace
$wgNamespacesWithSubpages[NS_MAIN] = true;

# Disable redirects to special pages and interwiki redirects, which use a 302 and have no "redirected from" link.
# http://www.mediawiki.org/wiki/Manual:%24wgDisableHardRedirects
$wgDisableHardRedirects = false;

$wgShowExceptionDetails = true;


# gestion du cache dans Mediawiki
$wgUseFileCache = true;

# Autorisation de téléverser certains types de fichiers
$wgFileExtensions[] = 'dwg';
$wgFileExtensions[] = 'dxf';
$wgFileExtensions[] = 'scad';

// Cellar S3 credentials
$wgAWSCredentials = [
	'key' => getenv("CELLAR_ADDON_KEY_ID"),
	'secret' => getenv("CELLAR_ADDON_KEY_SECRET"),
	'token' => false
];

//The url used for the API (PutObject, etc.)
$wgFileBackends['s3']['endpoint'] = 'https://' . getenv("CELLAR_ADDON_HOST");

$wgAWSRegion = 'default';

// Replace <something> with the name of your S3 bucket, e.g. wonderfulbali234.
$wgAWSBucketName = "cdn.bepo.fr";
$wgAWSBucketDomain = $wgAWSBucketName;
$wgAWSBucketTopSubdirectory="/images";


// Add Clever Cloud logo for sponsorship
$wgFooterIcons['hostedby'] = array(
	"clever-cloud" => [
		"src" => "https://www.clever-cloud.com/app/themes/Starter/assets/img/brand-assets/logo_on_white.svg",
		"url" => "https://www.clever-cloud.com/",
		"alt" => "Hébergement gracieusement offert par Clever Cloud",
	]
);

$wgSMTP = [
	'host'      => 'tls://mail.infomaniak.com',
	'IDHost'    => 'bepo.fr',
	'localhost' => 'bepo.fr',
	'port'      => 465,
	'auth'      => true,
	'username'  => $wgPasswordSender,
	'password'  => getenv("MW_SMTP_PASSWORD")
];
